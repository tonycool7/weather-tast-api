<?php
/**
 * Created by PhpStorm.
 * User: tonycool
 * Date: 8/6/2020
 * Time: 6:26 PM
 */

namespace App\Services;

use \Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

Abstract class AbstractService
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }
}
