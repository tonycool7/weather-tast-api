<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\MapLayerCreationRequest;
use App\Http\Requests\MapLocationSpecificRequest;
use App\Services\MapService;
use Illuminate\Http\Request;

class MapController extends Controller
{
    protected $mapService;

    public function __construct(MapService $mapService)
    {
        $this->mapService = $mapService;
    }

    public function fetchMapCreationLayer(MapLayerCreationRequest $request){
        $result = $this->mapService->fetchLayer($request);

        return response()->json(['data' => $result], 200);
    }

    public function fetchLocationSpecificValue(MapLocationSpecificRequest $request){
        $result = $this->mapService->fetchLocation($request);

        return response()->json(['data' => $result], 200);
    }
}
