<?php

namespace App\Exceptions;

use Illuminate\Validation\ValidationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Psy\Util\Json;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return JsonResponse
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        return response()->json([
            'error' => [
                'message' => $exception->getMessage(),
                'code' => 401
            ]
        ], 401);
    }

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->renderable(function (ValidationException $e) {
            return response()->json([
                'error' => [
                    'message' => $e->getMessage(),
                    'errors' => $e->validator->errors(),
                    'code' => 422
                ]
            ], 422);
        });

        $this->renderable(function (AuthenticationException $e) {
            return response()->json([
                'error' => [
                    'message' => $e->getMessage(),
                    'code' => $e->getCode()
                ]
            ], 401);
        });

        $this->renderable(function (LoginException $e) {
            return response()->json([
                'error' => [
                    'message' => $e->getMessage(),
                    'code' => $e->getCode()
                ]
            ], 422);
        });

        $this->renderable(function (HttpException $e) {
            return response()->json([
                'error' => [
                    'message' => "Access forbidden",
                    'code' => 403
                ]
            ], 403);
        });

        $this->renderable(function (ModelNotFoundException $e) {
            return response()->json([
                'error' => [
                    'message' => $e->getMessage(),
                    'code' => 404
                ]
            ], 404);
        });
        $this->renderable(function (QueryException $e) {
            return response()->json([
                'error' => [
                    'message' => $e->getMessage(),
                    'code' => 500
                ]
            ], 500);
        });
    }
}
