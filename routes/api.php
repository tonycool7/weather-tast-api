<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\V1\{
    UserController,
    MapController
};

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group(['middleware' => 'auth:api'], function(){

    Route::get('/users', [UserController::class, 'loadUsers']);

    Route::apiResource('/user', UserController::class);

    Route::get('/fetch_map_layer', [MapController::class, 'fetchMapCreationLayer']);

    Route::get('/fetch_location', [MapController::class, 'fetchLocationSpecificValue']);

    Route::post('/logout', [UserController::class, 'logout']);
});

Route::post('/register', [UserController::class, 'register']);

Route::post('/login', [UserController::class, 'login']);

Route::fallback(function(){
    return response()->json([
        'message' => 'Page Not Found. If error persists, contact if@africaplusworld.com'], 404);
});
