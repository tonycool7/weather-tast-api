<?php


namespace App\Services;


use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class MapService extends AbstractService
{
    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    public function api($url, $payload){
        $authKey = env('AUTH_KEY');

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload));

        //Disable CURLOPT_SSL_VERIFYHOST and CURLOPT_SSL_VERIFYPEER by
        //setting them to false.
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = "Auth-Key: {$authKey}";

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $responseData = curl_exec($ch);

        curl_close($ch);

        return json_decode($responseData);
    }

    public function fetchLayer($request){
        $payload = $request->all();

        $payload['day'] = intval($request->day);
        $payload['w'] = intval($request->w);
        $payload['h'] = intval($request->h);

        $url = "https://test.ignitia.se/api/fetch/plot/chorain";

        return $this->api($url, $payload);
    }

    public function fetchLocation($request){
        $payload = $request->all();

        $payload['day'] = intval($request->day);

        $url = "https://test.ignitia.se/api/fetch/parameter/chorain";

        return $this->api($url, $payload);
    }
}
