<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Services\UserService;
use Illuminate\Http\Request;
use \Illuminate\Http\JsonResponse;

class UserController extends Controller
{

    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
        $this->middleware('isAdmin')->only(['store', 'update', 'destroy']);
    }

    /**
     * @return JsonResponse
     */
    public function logout() : JsonResponse{
        $result = $this->userService->logout();

        return response()->json([
            'data' => true
        ], 200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index (Request $request) : JsonResponse{

        return response()->json([
            'data' => $request->user()
        ], 200);
    }

    /**
     * @param UserRequest $request
     * @return JsonResponse
     */
    public function register(UserRequest $request) : JsonResponse {
        $response = $this->userService->register($request);

        return response()->json([
            'data' => $response
        ], 201);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request) : JsonResponse {
        $response = $this->userService->login($request);

        return response()->json([
            'data' => $response
        ], 201);
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function loadUsers() : JsonResponse
    {
        $response = $this->userService->loadUsers();

        return response()->json([
            'data' => $response
        ], 200);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  UserRequest  $request
     * @return JsonResponse
     */
    public function store(UserRequest $request) : JsonResponse
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function show($id) : JsonResponse
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return JsonResponse
     */
    public function update(Request $request, $id) : JsonResponse
    {
        $response = $this->userService->userUpdate($request);

        return response()->json([
            'data' => $response
        ], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function destroy($id) : JsonResponse
    {
        $response = $this->userService->deleteUser($id);

        return response()->json([
            'data' => $response
        ], 201);

    }
}
