<?php


namespace App\Services;


use App\Exceptions\LoginException;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserService extends AbstractService
{

    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    public function deleteUser($id){
        $result = $this->model->findOrFail($id)->delete();

        return $result;
    }

    public function userUpdate($request){

        $input = $request->all();

        $user = $this->model->findOrFail($input['id']);

        try{

            $user->update($input);

        }catch (\Exception $e){
            return $e;
        }

        return $this->loadUsers();
    }

    public function loadUsers(){
        $users = $this->model->all();

        return $users;
    }

    public function register($request)
    {
        $user = null;
        $accessToken = null;

        try {

            $payload = $request->all();

            //Hash password
            $payload['password'] = Hash::make($payload['password']);

            //The first user to register is automatically a super admin
            $payload['isAdmin'] = User::all()->isEmpty() ? 1 : 0;

            $user = User::create($payload);

            //Generate token for user
            $accessToken = $user->createToken('weather-dashboard-api')->accessToken;

        } catch (\Exception $e) {
            return $e;
        }

        return  [
            "token" => $accessToken,
            "user" => $user
        ];
    }

    public function logout()
    {
        $userTokens = Auth::user()->tokens;

        foreach ($userTokens as $token) {

            $token->revoke();

        }

        return [];
    }

    public function login($request)
    {
        if (Auth::attempt(request(['email', 'password']))) {

            $user = Auth::user();

            $accessToken = $user->createToken('weather-dashboard-api')->accessToken;

            return [
                "token" => $accessToken,
                "user" => $user
            ];

        } else {
            throw new LoginException("Username or password incorrect", 401);
        }

    }

}
